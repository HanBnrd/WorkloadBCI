# Workload BCI #

<img align="right" width="100" height="100" src="logo.png">

**Title:** Workload BCI  
**Keywords:** fnirs, eeg, bci, real-time, mental workload, ros  
**Version:** 0.4  
**Authors:** Johann Benerradi  
**Web site:** [Mixed Reality Lab](https://www.nottingham.ac.uk/research/groups/mixedrealitylab/)  
**Platform:** Python 3  
**License:** GNU GPL 3.0  


[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.14410195.svg)](https://doi.org/10.5281/zenodo.14410195)


### Description
Real-time BCI and data acquisition (fNIRS & EEG) 


### Cite
If you are using this software, please cite [this reference](https://doi.org/10.5281/zenodo.14410195):
```
@software{benerradi2024workload,
  author={Benerradi, Johann},
  title={Workload BCI: software for real-time fNIRS and EEG experiments},
  year={2024},
  publisher={Zenodo},
  doi={10.5281/zenodo.14410195},
}
```


### Requires
- [Python 3](https://www.python.org/downloads/)  


### Setup OxySoft
- *Optional (not useful for LSL streaming): instruction to setup OxySoft for real-time ASCII data export can be found in the [manual](./info/manual.pdf)*
- Instruction to setup OxySoft for real-time LSL OD and ADC export can be found in [this video](https://www.youtube.com/watch?v=qgxrcykkve0); you can use [LabRecorder](https://github.com/labstreaminglayer/App-LabRecorder/releases) to check that the data is properly being sent via LSL


### Setup LSL communication between 2 machines
To read the LSL feed from the Windows machine on the Ubuntu machine, follow the steps below.

- Connect the 2 machines with an Ethernet cable

On the Windows machine:
- Open the Network and Sharing Center
- Click on the Ethernet connection
- Open Properties
- Click on Internet Protocol Version 4
- Open Properties
- Setup the Address: `192.168.5.5`
- Setup the Netmask: `255.255.255.0`
- Leave the rest blank
- The firewall may have to be turned off

On the Ubuntu machine:
- Open the Network Manager
- Add a new wired connection
- Click on the IPv4 tab
- Set the method to Manual
- Setup the Address: `192.168.5.10`
- Setup the Netmask: `255.255.255.0`
- Leave the rest blank
- Save

*Note: you can choose the IP address you like, the key thing is to have the 2 computers on the same subnet but with a different IP address.*


### Setup ROS
- 1) Install ROS Noetic on Ubuntu 20.04  
Follow the installation instruction on the official website: http://wiki.ros.org/noetic/Installation/Ubuntu.
Be sure to have the environment setup by checking that you have `source /opt/ros/noetic/setup.bash` in your `.bashrc` file. If not, type the following command in the terminal:
```bash
echo 'source /opt/ros/noetic/setup.bash' >> ~/.bashrc
source ~/.bashrc
```

- 2) Install Miniconda  
In the terminal:
```bash
cd ~
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh
```
Press Enter and answer "yes" to all.
Close the terminal and reopen a new one.

- 3) Install the libraries  
In the terminal:
```bash
conda install -c conda-forge liblsl
pip install rospy pyyaml rospkg empy pylsl
```

- 4) Create a ROS workspace and package  
In the terminal:
```bash
mkdir -p ~/ros_workspace/src
cd ~/ros_workspace/
catkin_make
cd src/
catkin_create_pkg robot_pkg rospy std_msgs std_srvs
echo 'source ~/ros_workspace/devel/setup.bash' >> ~/.bashrc
source ~/.bashrc
```

- 5) Get the ROS wrappers  
In the terminal:
```bash
roscd robot_pkg/src/
wget https://gitlab.com/HanBnrd/workload-bci/-/raw/master/src/scripts_ros/data_lsl_wrapper.py
wget https://gitlab.com/HanBnrd/workload-bci/-/raw/master/src/scripts_ros/event_lsl_wrapper.py
chmod +x *
```

- 6) Edit `CMakeLists.txt`  
Open `~/ros_workspace/src/robot_pkg/CMakeLists.txt`, uncomment the `catkin_install_python()` call and edit it so it looks like this:
```text
catkin_install_python(PROGRAMS
  src/data_lsl_wrapper.py src/event_lsl_wrapper.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
```
Then in the terminal:
```bash
cd ~/ros_workspace/
catkin_make
```

- 7) Run the wrapper nodes  
In the terminal:
```bash
rosrun robot_pkg data_lsl_wrapper.py
```
or
```bash
rosrun robot_pkg event_lsl_wrapper.py
```

*Note: if not working, you may have to open another terminal and run the `roscore` command (let it run in background).*


---
&copy; [*Johann Benerradi*](https://gitlab.com/HanBnrd)
