# -*- coding: utf-8 -*-

# Workload BCI
# Copyright (C)  Johann Benerradi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tkinter as tk
import tkinter.font as tkf
import time


def update(label, button):
    """
    Updating function
    """
    button.configure(text=' ... ')
    label.configure(text='LOW\n*', fg='#ffd944')
    label.update()
    time.sleep(2)
    label.configure(text='MEDIUM\n***', fg='#ff8244')
    label.update()
    time.sleep(2)
    label.configure(text='HIGH\n*****', fg='#ff5444')
    label.update()
    time.sleep(2)
    label.configure(text='N/A\n-', fg='#d9d9d9')
    button.configure(text='Start')


def main():
    # Notice
    print('Workload BCI')
    print('Copyright (C)  Johann Benerradi')
    print('This program comes with ABSOLUTELY NO WARRANTY.')
    print('This is free software, and you are welcome to')
    print('redistribute it under certain conditions.')
    print('---')

    # Set window
    root = tk.Tk()
    root.title('Workload BCI')
    root.configure(background='#292d34')

    # Define fonts
    title = tkf.Font(family='Courier', size=45, weight='bold')
    subtitle = tkf.Font(family='Courier', size=20)
    info = tkf.Font(family='Courier', size=20, weight='bold')
    text = tkf.Font(family='Courier', size=12, weight='bold')

    # Title
    label = tk.Label(root, text='Workload BCI', padx=40, pady=3, font=title,
                     bg='#292d34', fg='#d9d9d9')
    label.pack()

    # Real-time feedback frame
    lf = tk.LabelFrame(root, text='Real-time workload', font=subtitle, bd=3,
                       padx=20, pady=15, bg='#292d34', fg='#d9d9d9')
    lf.pack(fill='both', expand='yes')
    label = tk.Label(lf, text='N/A\n-', padx=40, pady=3, font=info,
                     bg='#292d34', fg='#d9d9d9')
    label.pack()
    button = tk.Button(lf, text='Start', font=text, padx=15, pady=15,
                       command=lambda: update(label, button),
                       bg='#3f3f3f', fg='#d9d9d9')
    button.pack()

    # Display window until it is closed
    root.mainloop()


if __name__ == '__main__':
    main()
