# -*- coding: utf-8 -*-

# Workload BCI
# Copyright (C)  Johann Benerradi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tkinter as tk
import tkinter.font as tkf
from tkinter import ttk
from pathlib import Path
from random import shuffle

import lib as lib


def run(entry_frequency, entry_duration, condition_name, progressbar,
        baseline=60):
    """
    Export function
    """
    frequency = int(entry_frequency.get())
    duration = int(entry_duration.get())
    total_duration = baseline + duration
    thread_export = lib.Export(frequency, total_duration, condition_name)
    thread_launcher = lib.Launcher(condition_name, baseline)
    thread_pb = lib.Progressbar(duration, progressbar, baseline)
    thread_export.start()
    thread_launcher.start()
    thread_pb.start()


def process(conditions, root):
    """
    Processing function
    """
    pr = lib.Process()
    pr.get_extremums()
    for condition in conditions:
        pr.normalize(condition)
        pr.average(condition)
    root.destroy()


def main():
    # Notice
    print('Workload BCI')
    print('Copyright (C)  Johann Benerradi')
    print('This program comes with ABSOLUTELY NO WARRANTY.')
    print('This is free software, and you are welcome to')
    print('redistribute it under certain conditions.')
    print('---')

    # Print conditions
    conditions = ['acquisition']
    print(conditions)

    # Set window
    root = tk.Tk()
    root.title('Workload BCI')
    root.configure(background='#292d34')

    # Define fonts
    title = tkf.Font(family='Courier', size=45, weight='bold')
    subtitle = tkf.Font(family='Courier', size=20)
    text = tkf.Font(family='Courier', size=12, weight='bold')

    # Title
    label = tk.Label(root, text='Workload BCI', padx=40, pady=3, font=title,
                     bg='#292d34', fg='#d9d9d9')
    label.pack()

    # Settings frame
    lf_settings = tk.LabelFrame(root, text='Settings', font=subtitle, bd=3,
                                padx=20, pady=15, bg='#292d34', fg='#d9d9d9')
    lf_settings.pack(fill='both', expand='yes')
    # Frequency entry
    l_frequency = tk.Label(lf_settings, text='Frequency (Hz) ', font=text,
                           bg='#292d34', fg='#d9d9d9')
    l_frequency.pack(side='left')
    sv_frequency = tk.StringVar(lf_settings, value='10')
    e_frequency = tk.Entry(lf_settings, textvariable=sv_frequency, width=6)
    e_frequency.pack(side='left')
    # Duration entry
    l_duration = tk.Label(lf_settings, text=' Duration (s)', font=text,
                          bg='#292d34', fg='#d9d9d9')
    l_duration.pack(side='right')
    sv_duration = tk.StringVar(lf_settings, value='120')
    e_duration = tk.Entry(lf_settings, textvariable=sv_duration, width=6)
    e_duration.pack(side='right')

    # Acquisition frame
    lf_acquisition = tk.LabelFrame(root, text='Acquisition', font=subtitle,
                                   bd=3, padx=20, pady=15, bg='#292d34',
                                   fg='#00e203')
    lf_acquisition.pack(fill='both', expand='yes')
    pb_acquisition = ttk.Progressbar(lf_acquisition, orient='horizontal',
                                     length=300, mode='determinate')
    button_acquisition = tk.Button(lf_acquisition, text='Run acquisition',
                                   font=text, padx=15, pady=15,
                                   command=lambda: run(e_frequency, e_duration,
                                                       'acquisition',
                                                       pb_acquisition),
                                   bg='#3f3f3f', fg='#d9d9d9')
    button_acquisition.pack()
    pb_acquisition.pack()
    pb_acquisition['maximum'] = 100
    if Path('../data/raw_acquisition.csv').exists():
        pb_acquisition['value'] = 100

    # Processing button
    f_process = tk.Frame(root, bd=3, padx=20, pady=5, bg='#292d34')
    f_process.pack(fill='both', expand='yes')
    button_launch = tk.Button(f_process, text='Process', font=text,
                              command=lambda: process(conditions, root),
                              padx=15, pady=5, bg='#3f3f3f', fg='#d9d9d9')
    button_launch.pack()

    # Display window until it is closed
    root.mainloop()


if __name__ == '__main__':
    main()
