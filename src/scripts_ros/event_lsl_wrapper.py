# ROS wrapper for reading LSL events from OxySoft
# © Johann Benerradi

import rospy
from std_msgs.msg import String
from std_srvs.srv import Trigger
from pylsl import StreamInlet, resolve_stream


class ROSLSLEventWrapper:
    def __init__(self, stream_name, sfreq):
        """
        Parameters
        ----------
        stream_name : string
            Name of the LSL stream to read.

        sfreq : float
            Sampling frequency to which the stream is read (in Hz).
        """
        data_stream = resolve_stream("name", stream_name)[0]
        self.data_inlet = StreamInlet(data_stream)

        rospy.Service("close_lsl_event", Trigger, self.callback_close)
        publisher_name = stream_name.lower().replace(" ", "_")
        self.lsl_pub = rospy.Publisher(publisher_name, String,
                                       queue_size=10)
        rospy.Timer(rospy.Duration(1.0/sfreq), self.publish_lsl_event)

    def publish_lsl_event(self, event=None):
        array = String()
        sample, timestamp = self.data_inlet.pull_sample()
        array = str([timestamp] + sample)
        rospy.loginfo(f"LSL event: {array}")
        self.lsl_pub.publish(array)

    def close(self):
        self.data_inlet.close_stream()

    def callback_close(self, req):
        self.close()
        return {"success": True, "message": "LSL event stream has been closed"}


if __name__ == "__main__":
    rospy.init_node("lsl_event_reader")
    lsl_wrapper = ROSLSLEventWrapper("OxySoft Event", 10.0)
    rospy.on_shutdown(lsl_wrapper.close)
    rospy.loginfo("LSL event reader is now started.")
    rospy.spin()
