# ROS publisher for LSL data from OxySoft
# © Johann Benerradi

import rospy
from std_msgs.msg import Float64MultiArray

from pylsl import StreamInlet, resolve_stream


def talker():
    data_stream = resolve_stream("name", "OxySoft")[0]
    data_inlet = StreamInlet(data_stream)
    info = data_inlet.info()
    sfreq = info.nominal_srate()
    pub = rospy.Publisher("lsl_data", Float64MultiArray, queue_size=10)
    rospy.init_node("talker", anonymous=True)
    rate = rospy.Rate(sfreq)
    while not rospy.is_shutdown():
        sample, timestamp = data_inlet.pull_sample()
        log_array = Float64MultiArray()
        log_array.data = [timestamp] + sample
        rospy.loginfo(log_array)
        pub.publish(log_array)
        rate.sleep()


if __name__ == "__main__":
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
