# ROS wrapper for reading LSL data from OxySoft
# © Johann Benerradi

import rospy
from std_msgs.msg import Float64MultiArray
from std_srvs.srv import Trigger
from pylsl import StreamInlet, resolve_stream


class ROSLSLDataWrapper:
    def __init__(self, stream_name):
        """
        Parameters
        ----------
        stream_name : string
            Name of the LSL stream to read.
        """
        data_stream = resolve_stream("name", stream_name)[0]
        self.data_inlet = StreamInlet(data_stream)
        info = self.data_inlet.info()
        sfreq = info.nominal_srate()

        rospy.Service("close_lsl_data", Trigger, self.callback_close)
        publisher_name = stream_name.lower().replace(" ", "_")
        self.lsl_pub = rospy.Publisher(publisher_name, Float64MultiArray,
                                       queue_size=10)
        rospy.Timer(rospy.Duration(1.0/sfreq), self.publish_lsl_data)

    def publish_lsl_data(self, event=None):
        array = Float64MultiArray()
        sample, timestamp = self.data_inlet.pull_sample()
        array.data = [timestamp] + sample
        rospy.loginfo(f"LSL data: {array.data}")
        self.lsl_pub.publish(array)

    def close(self):
        self.data_inlet.close_stream()

    def callback_close(self, req):
        self.close()
        return {"success": True, "message": "LSL data stream has been closed"}


if __name__ == "__main__":
    rospy.init_node("lsl_data_reader")
    lsl_wrapper = ROSLSLDataWrapper("OxySoft")
    rospy.on_shutdown(lsl_wrapper.close)
    rospy.loginfo("LSL data reader is now started.")
    rospy.spin()
