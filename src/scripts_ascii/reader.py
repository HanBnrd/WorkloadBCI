# Read multi-channel data (ASCII) from OctaMon on OxySoft
# © Johann Benerradi

import numpy as np
import socket
import time


HOST = "127.0.0.1"
PORT = 7777
BUFFER_SIZE = 1024  # must be more than the size of a line
N_SAMPLES = 10


def read_sample(connected_socket):
    data = connected_socket.recv(BUFFER_SIZE)

    # If nothing is recieved
    if not data:
        return False, None

    # Format the sample
    line = str(data)
    line = line.split("b'")[1]
    line = line.split("\\n'")[0]

    # If extra \n in the sample try to fix
    if "\\n" in line:
        line = line.split("\\n")[0]

    # Split the line
    line_split = line.split(", ")

    if len(line_split) != 17:
        return False, str(data)
    else:
        return True, line_split


# Connect to the port sending OxySoft ASCII
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    data = s.recv(BUFFER_SIZE)

    # Read headers
    success, sample = read_sample(s)
    if success:
        headers = sample
    else:
        print(f"WARNING: {sample}")

    # Read data
    for _ in range(N_SAMPLES):
        success, sample = read_sample(s)

        if not success:
            print(f"WARNING: {sample}")
            continue
        sample_number = np.array(sample[0])
        sample_number = sample_number.astype(int)
        sample_array = np.array(sample[1:])
        sample_array = sample_array.astype(float)
        print(sample_number, sample_array)
        time.sleep(0.5)
